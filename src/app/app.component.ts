import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SweetAlertService } from 'ng2-sweetalert2';
import { DataService } from '../shared/data.service';
import 'rxjs/add/operator/catch';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [DataService, SweetAlertService]
})
export class AppComponent implements OnInit {
  form: FormGroup;
  api = '';
  constructor(
    private http: HttpClient,
    private swal: SweetAlertService,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      initital_date: new FormControl('0000-00-00', [this.date]),
      service_date: new FormControl('0000-00-00')
    }
    );
  }
  date(control: FormControl) {
    const checkDate = new Date(control.value);
    const minDate = new Date('1901-01-01');
    const maxDate = new Date('2050-12-31');

    if ((minDate > checkDate)
    || (maxDate < checkDate)) {
      return {
        'dateError': true
      };
    }
    if (control.value === '' || control.value === '0000-00-00') {
      return {
        'dateError': true
      };
    }
    return null;
  }

  pushDate(form) {
    if (form.invalid) {
      const dateErr = form.controls.initital_date.errors['dateError'];
      if (dateErr) {
        this.swal.error({ title: 'Ошибка', text: 'Исходная дата отсутствует или не верна' });
      }
    } else {
      this.dataService.saveDate(form.value['initital_date']);
    }
  }
  pullDate() {
    const serviceDate = this.dataService.loadDate();
    if (serviceDate.length) {
      this.form.patchValue({ service_date: this.dataService.loadDate() });
    } else {
      this.swal.error({ title: 'Ошибка', text: 'Отсутствует дата в сервисе' });

    }
  }
  sendDate() {
    const date = this.form.get('service_date').value;
    if (date === '0000-00-00'){
      this.swal.error({ title: 'Ошибка', text: 'Отсутствует дата для отправки' });
    }
    const data = {date: date};
    this.http.post(`${this.api}/somecontroller/savedata`, JSON.stringify(data))
    .subscribe(
      res => console.log('success', res),
      error => console.log('err', error)
    );
  }
}
