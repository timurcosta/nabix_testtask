import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class DataService {
    storedDate = '';
    constructor() {
    }
    saveDate(date){
        this.storedDate = date;
        console.log(this.storedDate);

    }
    loadDate(){
        return this.storedDate;
    }

}
